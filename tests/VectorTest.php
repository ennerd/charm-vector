<?php
use PHPUnit\Framework\TestCase;
use Charm\Vector;
use OutOfBoundsException;
use OutOfRangeException;
/**
 * @covers Charm\Vector
 */
final class VectorTest extends TestCase {

    private Vector $instance;

    public function setUp(): void {
        $this->instance = new Vector();
        $this->assertInstanceOf(Vector::class, $this->instance);
    }

    public function testCount() {
        $this->assertSame(0, $this->instance->count());
        $this->assertSame(0, count($this->instance));

        $this->instance->push('Test1');
        $this->assertSame(1, count($this->instance));

        $this->instance[] = 'Test1.5';
        $this->assertSame(2, count($this->instance));

        $this->instance[2] = 'Test1.75';
        $this->assertSame(3, count($this->instance));

        $this->instance->push('Test2');
        $this->assertSame(4, count($this->instance));

        $this->instance->unshift('Test1');
        $this->assertSame(5, count($this->instance));

        $this->instance->unshift('Test2');
        $this->assertSame(6, count($this->instance));

        $this->instance->push('Finally');
        $this->instance->shift();
        $this->assertSame(6, count($this->instance));

        $this->instance->pop();
        $this->assertSame(5, count($this->instance));

        unset($this->instance[count($this->instance)-1]);
        $this->assertSame(4, count($this->instance));

        unset($this->instance[0]);
        $this->assertSame(3, count($this->instance));
    }

    /**
     * @covers ::offsetSet
     * @covers ::count
     */
    public function testArrayAccessInsertVariousTypes() {
        $this->instance[] = 0;
        $this->instance[1] = 0.0;
        $this->instance[] = false;
        $this->instance[3] = true;
        $this->instance[] = null;
        $this->instance[] = "string";
        $this->instance[6] = ($vectorInstance = new Vector());
        $this->assertEquals(7, count($this->instance));
        $this->assertSame(0, $this->instance[0]);
        $this->assertSame(0.0, $this->instance[1]);
        $this->assertSame(false, $this->instance[2]);
        $this->assertSame(true, $this->instance[3]);
        $this->assertSame(null, $this->instance[4]);
        $this->assertSame("string", $this->instance[5]);
        $this->assertSame($vectorInstance, $this->instance[6]);
        $this->assertSame(0, $this->instance->shift());
        $this->assertSame($vectorInstance, $this->instance->pop());
    }

    public function testOutOfBoundsIsset() {
        $this->instance[] = 0;
        $this->instance[] = 1;
        $this->assertFalse(isset($this->instance[-1]));
        $this->assertTrue(isset($this->instance[0]));
        $this->assertTrue(isset($this->instance[1]));
        $this->assertFalse(isset($this->instance[2]));
    }

    public function testIterateAfterPushPopUnshiftShift() {
        $this->instance->push(1);
        $this->instance->push(2);
        $this->instance->push(3);
        $this->instance->push(4);
        $this->instance->push(5);
        $this->instance->push(6);
        $this->instance->push(7);
        $this->instance->push(8);
        $this->assertSame(1, $this->instance->shift());
        $this->assertSame(8, $this->instance->pop());
        $this->assertSame([2,3,4,5,6,7], iterator_to_array($this->instance));
        $this->instance->unshift(0);
        $this->instance->unshift(1);
        $this->instance->push(3);
        $this->instance->push(4);
        $this->assertSame([1,0,2,3,4,5,6,7,3,4], iterator_to_array($this->instance));
    }

    public function testEmptyPop() {
        $this->instance->push(123);
        $this->instance->pop();
        $this->assertSame(NULL, $this->instance->pop());
    }

    public function testEmptyShift() {
        $this->instance->push(123);
        $this->instance->pop();
        $this->assertSame(NULL, $this->instance->shift());
    }

    public function testExpectedExceptions() {
        $this->instance->push('Hello');
        $this->instance->push('World');

        // ArrayAccess should throw OutOfBoundsException for these
        try {
            $this->assertSame(null, $this->instance[20]);
        } catch (\Throwable $e) {
            $this->assertInstanceOf(OutOfBoundsException::class, $e);
        }

        try {
            $this->assertSame(null, $this->instance[-20]);
        } catch (\Throwable $e) {
            $this->assertInstanceOf(OutOfBoundsException::class, $e);
        }

        try {
            $this->assertSame(null, $this->instance["Hello World"]);
        } catch (\Throwable $e) {
            $this->assertInstanceOf(OutOfRangeException::class, $e);
        }
    }

}
