<?php
require 'vendor/autoload.php';

const TEST_SIZE = 50000;

function test_build_array() {
    $a = [];
    for ($i = 0; $i < TEST_SIZE; $i++) {
        $a[$i] = "STRING $i";
    }
    return $a;
}

function test_build_rarray() {
    $a = [];
    for ($i = 0; $i < TEST_SIZE; $i++) {
        array_unshift($a, "STRING $i");
    }
    return $a;
}

function test_build_vector() {
    $v = new Charm\Vector();
    for ($i = 0; $i < TEST_SIZE; $i++) {
        $v[] = "STRING $i";
    }
    return $v;
}

function test_build_rvector() {
    $v = new Charm\Vector();
    for ($i = 0; $i < TEST_SIZE; $i++) {
        $v->unshift("STRING $i");
    }
    return $v;
}

function test_build_ds_vector() {
    if (!class_exists(\Ds\Vector::class)) {
        return null;
    }
    $dv = new Ds\Vector(["Hei"]);
    for ($i = 0; $i < TEST_SIZE; $i++) {
        $dv[] = "STRING $i";
    }
    return $dv;
}

function test_build_rds_vector() {
    if (!class_exists(\Ds\Vector::class)) {
        return null;
    }
    $dv = new Ds\Vector();
    for ($i = 0; $i < TEST_SIZE; $i++) {
        $dv->unshift("STRING $i");
    }
    return $dv;
}

function shift_array(&$a) {
    for ($i = 0; $i < TEST_SIZE; $i++) {
        array_shift($a);
    }
}

function shift_vector($v) {
    for ($i = 0; $i < TEST_SIZE; $i++) {
        $v->shift();
    }
}

function shift_ds_vector($dv) {
    for ($i = 0; $i < TEST_SIZE; $i++) {
        $dv->shift();
    }
}

t();
$vec = test_build_vector();
t("VECTOR APPEND CREATE");
shift_vector($vec);
t("VECTOR SHIFT ALL");
$vec = test_build_rvector();
t("VECTOR PREPEND CREATE");

echo "\n\n";

$dvec = test_build_ds_vector();
t("DS\VECTOR APPEND CREATE");
shift_ds_vector($dvec);
t("DS\VECTOR SHIFT ALL");
$dvec = test_build_rds_vector();
t("DS\VECTOR PREPEND CREATE");

echo "\n\n";

$arr = test_build_array();
t("ARRAY APPEND CREATE");
shift_array($arr);
t("ARRAY SHIFT ALL");
$arr = test_build_rarray();
t("ARRAY PREPEND CREATE");
echo "\n\n";

function t(string $description=null) {
    static $ts = null;
    static $mem = null;
    if ($description === null) {
        $ts = microtime(true);
        $mem = memory_get_usage();
        return;
    }
    $t = microtime(true);
    $m = memory_get_usage();
    echo $description.": ".($t - $ts)." seconds ".floor(($m - $mem) / 1024)." KB mem\n";
    $ts = $t;
    $mem = $m;
}
