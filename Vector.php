<?php
namespace Charm;

use Charm\Vector\VectorInterface;
use Charm\Vector\Traits\ArrayAccess;
use Generator;
use OutOfBoundsException;
use OutOfRangeException;

/**
 * @template T
 * @package Charm
 */
class Vector implements VectorInterface {

    /**
     * Array used when prepending items when there is no positive integer space below
     * 0.
     * 
     * @var T[]
     */
    private array $_pre = [];
    /**
     * Array used when appending items when there is no negative integer space below
     * 0.
     * 
     * @var T[]
     */
    private array $_post = [];
    private int $_low = 0;
    private int $_high = 0;

    /**
     * Get the first element in the vector without removing it.
     * 
     * @return T|null 
     */
    public function bottom(): mixed {
        return $this->get(0);
    }

    /**
     * Get the last element in the vector without removing it.
     * 
     * @return T|null 
     */
    public function top(): mixed {
        $offset = $this->count() - 1;
        if ($offset < 0) {
            return null;
        }
        return $this->get($offset);
    }

    /**
     * Enqueue an item to the vector, when using the vector as
     * a queue. This method is an alias of {@see VectorInterface::push()}
     * 
     * @param T $value 
     * @return void 
     */
    public function enqueue(mixed $value): void {
        $this->push($value);
    }

    /**
     * Dequeue an item from the vector, when using the vector as
     * a queue. This method is an alias of {@see VectorInterface::shift()}
     * 
     * @return T|null 
     */
    public function dequeue(): mixed {
        return $this->shift();
    }

    /**
     * Get the number of elements contained in the vector
     * 
     * @return int<0, max>
     */
    public function count(): int {
        return $this->_high - $this->_low;
    }

    /**
     * Iterate over the elements in the vector.
     * 
     * @return Generator<int,T> 
     */
    public function getIterator(): Generator {
        for ($i = $this->_low + 1; $i < $this->_high && $i <= 0; $i++) {
            yield $this->_pre[-$i];
        }
        for ($i = max(0, $this->_low); $i < $this->_high; $i++) {
            yield $this->_post[$i];
        }
    }

    /**
     * Get an item by its offset.
     * 
     * @param int<0,max> $offset 
     * @return T|null 
     * @throws OutOfBoundsException
     */
    public function &get(int $offset): mixed {
        if ($offset < 0) {
            $this->throwOutOfBoundsException($offset);
        }
        $offset = $this->_low + $offset;
        if ($offset >= $this->_high) {
            $this->throwOutOfBoundsException(-$this->_low + $offset);
        }
        if ($offset < 0) {
            $offset = -$offset - 1;
            return $this->_pre[$offset];
        }
        return $this->_post[$offset];
    }

    /**
     * Overwrite, prepend or append an item in the vector.
     * 
     * @param int<-1,max> $offset 
     * @param T $value 
     * @return void 
     */
    public function set(int $offset, mixed $value): void {
        if ($offset === $this->_high - $this->_low) {
            $this->push($value);
            return;
        }
        if ($offset === -1) {
            $this->unshift($value);
            return;
        }
        if ($offset < 0 || $offset >= ($this->_high - $this->_low)) {
            $this->throwOutOfBoundsException($offset);
        }
        $offset = $this->_low + $offset;
        if ($offset < 0) {
            $this->_pre[-$offset - 1] = $value;
        } else {
            $this->_post[$offset] = $value;
        }
    }

    /**
     * Check if the offset is contained within the vector.
     * 
     * @param int $offset 
     * @return bool 
     */
    public function has(int $offset): bool {
        return $offset >= 0 && $offset < ($this->_high - $this->_low);
    }

    /**
     * Delete an item from the vector, shifting all remaining 
     * items in the vector to the left.
     * 
     * @param int<0,max> $offset 
     * @return void 
     */
    public function delete(int $offset): void {
        if ($offset === 0) {
            $this->shift();
        } elseif ($offset === $this->count() - 1) {
            $this->pop();
        } elseif ($offset >= 0 || $offset < ($this->_high - $this->_low)) {
            $offset = $this->_low + $offset;
            if ($offset < 0) {
                array_splice($this->_pre, -$offset - 1, 1);
                $this->_low++;
            } else {
                array_splice($this->_post, $offset, 1);
                $this->_high--;
            }
        } else {
            $this->throwOutOfBoundsException($offset);
        }
    }

    /**
     * Append an item to the end of the vector.
     * 
     * @param T $value 
     * @return mixed 
     */
    public function push(mixed $value): void {
        if ($this->_high === $this->_low) {
            // take this opportunity to reset any skewing of offsets
            $this->_high = 0;
            $this->_low = 0;
        }
        if ($this->_high < 0) {
            $preOffset = -$this->_high++ - 1;
            $this->_pre[$preOffset] = $value;
        } else {
            $this->_post[$this->_high++] = $value;
        }
    }

    /**
     * Remove and return an item from the end of the vector.
     * 
     * @return T|null 
     */
    public function pop(): mixed {
        if ($this->_high === $this->_low) {
            return null;
        }
        if ($this->_high > 0) {
            $result = $this->_post[--$this->_high];
            unset($this->_post[$this->_high]);
        } else {
            $preOffset = -$this->_high--;
            $result = $this->_pre[$preOffset];
            unset($this->_pre[$preOffset]);
        }
        return $result;
    }

    /**
     * Remove and return an item from the beginning of the vector.
     * 
     * @return T|null 
     */
    public function shift(): mixed {
        if ($this->_high === $this->_low) {
            return null;
        }
        if ($this->_low < 0) {
            $preOffset = -$this->_low++ - 1;
            $result = $this->_pre[$preOffset];
            unset($this->_pre[$preOffset]);
        } else {
            $result = $this->_post[$this->_low];
            unset($this->_post[$this->_low++]);
        }
        return $result;
    }

    /**
     * Prepend an item to the beginning of the vector.
     * 
     * @param T $value 
     * @return mixed 
     */
    public function unshift(mixed $value) {
        if ($this->_high === $this->_low) {
            $this->_high = 1;
            $this->_low = 0;
            $this->_post[0] = $value;
            return;
        }
        if ($this->_low > 0) {
            $this->_post[--$this->_low] = $value;
        } else {
            $preOffset = -$this->_low--;
            $this->_pre[$preOffset] = $value;
        }
    }

    /**
     * Get an array representation of the vector.
     * 
     * @return T[]
     */
    public function jsonSerialize() {
        return iterator_to_array($this);
    }

    private function throwOutOfBoundsException(int $offset) {
        throw new OutOfBoundsException("Offset $offset is out of bounds for this Vector. Valid offsets is 0-".$this->count());
    }

    public function &offsetGet($offset): mixed {
        if (!is_numeric($offset)) {
            throw new OutOfRangeException("The offset '$offset' is invalid");
        }
        return $this->get($offset);
    }

    /**
     * 
     * @param int $offset 
     * @param T $value 
     * @return void 
     * @throws OutOfRangeException 
     * @throws OutOfBoundsException 
     */
    public function offsetSet($offset, $value): void {
        if ($offset !== null && !is_numeric($offset)) {
            throw new OutOfRangeException("The offset '$offset' is invalid");
        }
        if ($offset === null) {
            $this->push($value);
        } else {
            $this->set($offset, $value);
        }
    }

    /**
     * 
     * @param int $offset 
     * @return bool 
     */
    public function offsetExists($offset): bool {
        return $offset >= 0 && $offset < $this->count();
    }

    /**
     * 
     * @param int $offset 
     * @return void 
     * @throws OutOfRangeException 
     * @throws OutOfBoundsException 
     */
    public function offsetUnset($offset): void {
        if (!is_numeric($offset)) {
            throw new OutOfRangeException("The offset '$offset' is invalid");
        }
        $this->delete($offset);
    }

}
