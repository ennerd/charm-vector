<?php
namespace Charm\Vector;

use OutOfBoundsException;

/**
 * @package Charm\Vector
 */
interface VectorInterface extends \IteratorAggregate, \ArrayAccess, \Countable, \JsonSerializable {
    public function bottom(): mixed;
    public function top(): mixed;
    public function get(int $offset): mixed;
    public function set(int $offset, mixed $value): void;
    public function has(int $offset): bool;
    public function delete(int $offset): void;
    public function enqueue(mixed $value): void;
    public function dequeue(): mixed;
    public function push(mixed $value);
    public function pop(): mixed;
    public function shift(): mixed;
    public function unshift(mixed $value);
}
